/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2018 Konsulko Group
 * Copyright (C) 2019 FUJITSU LIMITED
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtCore/QDebug>
#include <QtGui/QGuiApplication>
#include <QtCore/QUrl>

#include <QtQml/QQmlApplicationEngine>

#define DEFAULT_QT_QPA_PLATFORM "eglfs"
#define DEFAULT_EGLFS_CONFIG_FILE "/etc/kms.conf"

int main(int argc, char *argv[])
{
    int i;
    for (i = 1; i < argc; i++) {
        if (!argv[i] || *argv[i] != '-')
            continue;
        const char *arg = argv[i];
        if (arg[1] == '-') // startsWith("--")
            ++arg;
        if (strcmp(arg, "-platform") == 0)
            break;
    }

    /* Using "eglfs" as default if QT_QPA_PLATFORM is not set. */
    QByteArray platformNameEnv = qgetenv("QT_QPA_PLATFORM");
    if (platformNameEnv.isEmpty() && i == argc) {
        qputenv("QT_QPA_PLATFORM", DEFAULT_QT_QPA_PLATFORM);
    }

    /* Using "/etc/kms.conf" as default if QT_QPA_EGLFS_KMS_CONFIG/QT_QPA_KMS_CONFIG are not set. */
    if (qgetenv("QT_QPA_KMS_CONFIG").isEmpty() &&
        qgetenv("QT_QPA_EGLFS_KMS_CONFIG").isEmpty()) {
            qputenv("QT_QPA_EGLFS_KMS_CONFIG", DEFAULT_EGLFS_CONFIG_FILE);
    }

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine appEngine(QUrl("qrc:///cluster-gauges.qml"));

    return app.exec();
}
